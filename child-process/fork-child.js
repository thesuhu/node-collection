process.on('message', (msg) => {
    console.log('Message from parent:', msg);
});

let counter = 0;

setInterval(() => {
    process.send({
        counter: counter++
    });

    if (counter == 10) {
      process.exit()
    }
}, 1000);