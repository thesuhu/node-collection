// const unzipper = require('unzipper');

// (async() => {
//     try {
//         const directory = await unzipper.Open.file('./temp/test-unzip.zip')
//         const extracted = await directory.files[0].buffer('elsafrozen')

//         // If the extracted entity is a file,
//         // converting the extracted buffer to string would print the file content
//         console.log(extracted.toString())
//     } catch (err) {
//         console.log(err)
//     }
// })()

const unzipper = require('unzipper')
const basedir = require('app-root-path')

const zipfile = basedir + '/zipunzip/temp/test-unzip.zip'
const zippassword = 'elsafrozen'

unzip(zipfile, zippassword)

async function unzip(zipfile, password) {
    try {
        const directory = await unzipper.Open.file(zipfile)
        const extracted = await directory.files[0].buffer(password)

        // If the extracted entity is a file,
        // converting the extracted buffer to string would print the file content
        console.log(extracted.toString())
    } catch (err) {
        console.log(err.message)
    }
}