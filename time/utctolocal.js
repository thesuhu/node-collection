const moment = require('moment')
const tz = require('moment-timezone')

let utcTime = '2020-07-01T11:05:41.378Z'
let localTime = moment(utcTime).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')

console.log(utcTime)
console.log(localTime)