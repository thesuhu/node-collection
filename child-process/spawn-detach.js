const {
    spawn
} = require('child_process');

const child = spawn('node', ['./child-process/timer'], {
    detached: true,
    stdio: 'ignore'
});

child.unref();