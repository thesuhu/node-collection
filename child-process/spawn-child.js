// The child_process module enables us to access Operating System functionalities by running any system command inside a, well, child process.
const {
    spawn
} = require('child_process')

// const child = spawn('pwd')

// // console.log(child)

// // do something when the child process exits by registering a handler for the exit event
// child.on('exit', function (code, signal) {
//   console.log('child process exited with ' +
//               `code ${code} and signal ${signal}`)
// })

// // Most importantly, on the readable streams, we can listen to the data event
// child.stdout.on('data', (data) => {
//   console.log(`child stdout:\n${data}`);
// });

// child.stderr.on('data', (data) => {
//   console.error(`child stderr:\n${data}`);
// });

const child = spawn('find', ['.', '-type', 'f']);

child.stdout.on('data', (data) => {
  console.log(`child stdout:\n${data}`);
});

child.stderr.on('data', (data) => {
  console.error(`child stderr:\n${data}`);
});

child.on('exit', function (code, signal) {
  console.log('child process exited with ' +
              `code ${code} and signal ${signal}`)
})

