const { Parser } = require('json2csv')
const ftp = require('basic-ftp')
const streamifier = require('streamifier')

const ftpconfig = {
    host: '10.242.xxx.xxx',
    port: '21',
    user: 'xxx',
    password: 'xxx',
    secure: false
}

proses()

async function proses() {
    console.log('start')

    // contoh json
    var person = [
        {
            "nama": "Jananto Sigit",
            "alamat": "Bekasi",
            "usia": "38"
        },
        {
            "nama": "Rudi Hartono",
            "alamat": "Cirebon",
            "usia": "36"
        },
        {
            "nama": "Alan Nurfitra",
            "alamat": "Jakarta",
            "usia": "26"
        },
        {
            "nama": "Umar Jati",
            "alamat": "Depok",
            "usia": "40",
        }
    ]

    // create instance
    const json2csvParser = new Parser({ delimiter: '|', quote: '', header: false })
    // parse ke string csv
    const csv = json2csvParser.parse(person)

    console.log(typeof csv)
    console.log(csv)

    console.log('before')
    // convert string ke stream
    var buf = Buffer.from(csv, 'utf8')
    console.log(buf)

    // upload ke ftp
    var filename = 'fileupload.txt'
    var remotepath = '/2026/'

    var client = new ftp.Client()
    client.ftp.verbose = false
    try {
        await client.access(ftpconfig)
        // buffer harus diconvert ke readableStream dulu
        // jsftp menggunakan buffer sedangkan basic-ftp menggunakan readableStream		
        await client.ensureDir(remotepath)
        // await client.clearWorkingDir() 
        await client.uploadFrom(streamifier.createReadStream(buf), remotepath + filename)
        // await client.uploadFrom(filestream, remotepath + filename)

        console.log('upload sukses')
    } catch (e) {
        console.log(e)
    }
    client.close()

    console.log('finish')
}


// upload(streamifier.createReadStream(buf), '/2026/', 'tes_stream.txt')

// console.log('finish')

// // async funtion below

// async function upload(filestream, remotepath, filename) {
//     var client = new ftp.Client()
//     client.ftp.verbose = false
//     try {
//         await client.access(ftpconfig)
//         // buffer harus diconvert ke readableStream dulu
//         // jsftp menggunakan buffer sedangkan basic-ftp menggunakan readableStream		
//         // await client.uploadFrom(streamifier.createReadStream(buffer), remotepath + 'Error_SQL_20200512.log')
//         await client.ensureDir(remotepath)
//         await client.clearWorkingDir()
//         await client.uploadFrom(filestream, remotepath + filename)

//         console.log('upload sukses')
//     } catch (e) {
//         console.log(e)
//     }
//     client.close()
// }