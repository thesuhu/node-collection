const fs = require('fs');
const pdfmake = require('pdfmake/build/pdfmake.js');

// contoh membuat pdf
let PdfPrinter = require('pdfmake');

var fonts = {
  Roboto: {
    normal: './fonts/Roboto-Regular.ttf',
    bold: './fonts/Roboto-Medium.ttf',
    italics: './fonts/Roboto-Italic.ttf',
    bolditalics: './fonts/Roboto-MediumItalic.ttf'
  }
};

let printer = new PdfPrinter(fonts);

let docDefinition = {
  content: [
    { text: 'Bidadari Penyelamat', style: 'header', alignment: 'left', fontSize: 18 },
    '\n',
    {
      text: `6.30 pagi
      Aku sudah terbangun
      Matahari bersinar
      Tapi tak terasa panas
      Setan dalam hati tertawa
      Teringat kejadian semalam
      Semua bilang aku salah jalan
      Tapi nggak ada satupun yang punya peta
      Haripun berlalu
      Tak sadar sudah tahunan
      Kemana bidadariku pergi
      Yang bisa menyelamatkanku
      Seharusnya dia dari tadi
      Sudah berada disini
      Malaikat di jiwa terbelenggu
      Akal sehatku pun mati
      Semua orang coba-cobai mau menolong
      Tapi terdengar seperti menggonggong
      Haripun berlalu
      Aku sudah kecebur semakin dalam`, alignment: 'left', fontSize: 12
    },
    '\n',
    { text: 'Slank - Album Minoritas - 1994', style: ['footer', 'anotherStyle'] },
  ],
  styles: {
    header: {
      fontSize: 18,
      bold: true
    },
    footer: {
      fontSize: 12,
    },
    anotherStyle: {
      italics: true,
      alignment: 'left'
    }
  }
};

const pdfDoc = printer.createPdfKitDocument(docDefinition);
pdfDoc.pipe(fs.createWriteStream('./temp/lirik.pdf')); // save to file
// pdfDoc.pipe(process.stdout); // print to console
pdfDoc.end();