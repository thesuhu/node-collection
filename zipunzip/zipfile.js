const fs = require('fs')
const archiver = require('archiver')

async function zip(outputZipFile, password) {
    archiver.registerFormat('zip-encryptable', require('archiver-zip-encryptable'))
    var output = fs.createWriteStream(outputZipFile)
    var archive = archiver('zip-encryptable', {
        zlib: {
            level: 9
        },
        forceLocalTime: true,
        password: password
    })

    archive.pipe(output)

    archive.append(fs.readFileSync('./zipunzip/temp/helloworld1.txt', 'utf8'), {
        name: 'helloworld_1.txt'
    })
    archive.append(fs.readFileSync('./zipunzip/temp/helloworld2.txt', 'utf8'), {
        name: 'helloworld_2.txt'
    })

    // archive.append(Buffer.from('Hello World'), {
    //     name: 'test.txt'
    // });
    // archive.append(Buffer.from('Good Bye'), {
    //     name: 'test2.txt'
    // });

    archive.finalize()
}

(async() => {
    zip('./zipunzip/temp/test-zip.zip', 'pwd')
})()