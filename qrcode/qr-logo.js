const QRLogo = require('qr-with-logo');

const data = JSON.stringify({
    name: "Zacharie Happel",
    job: "Student/Intern",
    grade: "Senior"
});

const opts = {
    errorCorrectionLevel: 'H',
    rendererOpts: { quality: 0.3 }
};

// (async() => {
//     await QRLogo.generateQRWithLogo(data, "./qrcode/icon64.png", {}, "PNG", "./temp/qrlogo.png");
//     console.log('Done')
// })() // tidak terbaca scan

(async() => {
    // await QRLogo.generateQRWithLogo(data, "./qrcode/icon64.png", opts, "PNG", "./temp/qrlogo.png");
    console.log(await QRLogo.generateQRWithLogo(data, "./qrcode/icon64.png", opts, "PNG", "./temp/qrlogo.png"))
    console.log('Done')
})() // terbaca scan