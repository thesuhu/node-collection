const ftp = require('basic-ftp')
const basedir = require('app-root-path')

const ftpconfig = {
    host: '10.242.231.196',
    port: '21',
    user: 'ppnpndev',
    password: 'devppnpn',
    secure: false
}

async function downloadftp(localfile, remotefile) {
    var client = new ftp.Client()
    client.ftp.verbose = false
    try {
        await client.access(ftpconfig)
        console.log(await client.list())
        await client.downloadTo(localfile, remotefile)
    } catch (err) {
        console.log(err.message)
    }
}

// downloadftp('./temp/README-local.md', '/migrasi/2021/README.md', ).then(() => {
//     process.exit()
// })

downloadftp('./temp/PPNPN_654598_2021.zip', '/migrasi/2021/PPNPN_654598_2021.zip').then(() => {
    process.exit()
})