var QRCode = require('qrcode')

QRCode.toDataURL('I am a pony!', function(err, url) {
    console.log(url)
})

QRCode.toFile('./qrcode/filename.png', 'Some text', {
    color: {
        dark: '#00F', // Blue dots
        light: '#0000' // Transparent background
    }
}, function(err) {
    if (err) throw err
    console.log('done')
})