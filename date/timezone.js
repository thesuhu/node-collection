const mtz = require('moment-timezone')

var tz = () => {
    return mtz().tz('Asia/Jakarta').format('DD/MMM/YYYY hh:mm:ss Z')
}

console.log(tz())
console.log(mtz().format('DD MMMM YYYY hh:mm:ss Z'))