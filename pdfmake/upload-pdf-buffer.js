// const fs = require('fs');
const pdfmake = require('pdfmake/build/pdfmake.js');
// const pdffont = require('pdfmake/build/vfs_fonts.js');
const request = require('request');

// pembuatan buffer pdf
// pdfmake.vfs = pdffont.pdfMake.vfs;
let docDefinition = {
    content: [
        {
            text: 'A Simple PDF File',
            style: 'header',
            alignment: 'left',
            fontSize: 18
        },
        '\n',
        'Ini adalah file PDF sederhana yang dihasilkan oleh PDFMake untuk test',
        {
            text: `Test lebih banyak teks. lebih banyak teks. lebih banyak teks. lebih banyak teks. banyak lagi
            teks. lebih banyak teks. Membosankan, zzzz. lebih banyak teks. lebih banyak teks. Dan
            lebih banyak teks. lebih banyak teks. lebih banyak teks. lebih banyak teks. lebih banyak teks.
            lebih banyak teks. lebih banyak teks.`,
            alignment: 'left',
            fontSize: 12
        },
        '\n',
        {
            text: 'Test lebih banyak teks. lebih banyak teks. lebih banyak teks',
            style: ['footer', 'anotherStyle']
        },
        '\n',
        new Date().toLocaleString()
    ],
    styles: {
        header: {
            fontSize: 18,
            bold: true
        },
        footer: {
            fontSize: 12,
        },
        anotherStyle: {
            italics: true,
            alignment: 'left'
        }
    }
};

async function uploadBuffer(docDefinition) {
    let PdfPrinter = require('pdfmake');

    var fonts = {
        Roboto: {
            normal: './fonts/Roboto-Regular.ttf',
            bold: './fonts/Roboto-Medium.ttf',
            italics: './fonts/Roboto-Italic.ttf',
            bolditalics: './fonts/Roboto-MediumItalic.ttf'
        }
    };

    let printer = new PdfPrinter(fonts);
    const pdfMake = printer.createPdfKitDocument(docDefinition);

    let chunks = [];

    pdfMake.on("data", chunk => {
        chunks.push(chunk);
    });

    pdfMake.on("end", () => {
        let result = Buffer.concat(chunks);
        // console.log(result);
        let options = {
            'method': 'POST',
            'url': 'http://10.242.104.99/service/ds/bsre/api/v1/sign/pdf',
            'headers': {
                'Content-Type': 'multipart/form-data'
            },
            formData: {
                'file': {
                    'value': result,
                    'options': {
                        'filename': 'mypdf.pdf',
                        'contentType': null
                    }
                },
                'nik': '1234567890123456',
                'passphrase': 'pwdpwdpwd'
            }
        };
        // console.log(options);
        console.log('Uploading...');
        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
        });
    });

    pdfMake.end();

    // try {
    //     var request = require('request');
    //     var fs = require('fs');
    //     var options = {
    //         'method': 'POST',
    //         'url': 'http://localhost:3000/api/v1/sign/pdf',
    //         'headers': {
    //             'Content-Type': 'multipart/form-data'
    //         },
    //         formData: {
    //             'file': {
    //                 'value': fs.createReadStream('./temp/mypdf.pdf'),
    //                 'options': {
    //                     'filename': './temp/mypdf.pdf',
    //                     'contentType': null
    //                 }
    //             },
    //             'nik': '1234567890123456',
    //             'passphrase': 'pwdpwdpwd'
    //         }
    //     };
    //     request(options, function (error, response) {
    //         if (error) throw new Error(error);
    //         console.log(response.body);
    //     });
    // } catch (error) {
    //     console.log(error);
    // }

}

uploadBuffer(docDefinition);