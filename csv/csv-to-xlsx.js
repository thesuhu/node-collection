// // const path = require('path');
// const convertCsvToXlsx = require('@aternus/csv-to-xlsx');

// // let source = path.join(__dirname, 'report.csv');
// // let destination = path.join(__dirname, 'converted_report.xlsx');

// let source = 'd:/Temporary/test.csv'
// let destination = 'd:/Temporary/converted_test.xlsx'

// try {
//     convertCsvToXlsx(source, destination);
// } catch (e) {
//     console.error(e.toString());
// }

const xlsx = require('xlsx')
const csv = require('csv-parse/lib/sync');
const fs = require('fs')

var source = 'd:/Temporary/test.csv'
const csvFile = fs.readFileSync(source, 'UTF-8');
var destination = 'd:/Temporary/converted_test.xlsx'

// csv parser options
const csvOptions = {
    columns: true,
    delimiter: '|',
    ltrim: true,
    rtrim: true,
}

// get records
const records = csv(csvFile, csvOptions)
console.log(records)

// prepare the xlsx workbook
const wb = xlsx.utils.book_new()

// insert the records as a sheet
const ws = xlsx.utils.json_to_sheet(records);
xlsx.utils.book_append_sheet(wb, ws);

// write the xlsx workbook to destination
xlsx.writeFile(wb, destination);