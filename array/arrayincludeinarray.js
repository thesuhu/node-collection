const arr1 = ['pizza', 'cola'];
const arr2 = ['pizza', 'cake', 'cola'];

const containsAll = arr1.every(element => {
  return arr2.includes(element);
});

console.log(containsAll); // 👉️ true

const arr3 = ['pizzaZ', 'colaX'];
const arr4 = ['pizza', 'cake', 'cola'];

const containsSome = arr3.some(element => {
  return arr4.includes(element);
});

console.log(containsSome); // 👉️ true