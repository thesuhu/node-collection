const { encrypt, decrypt } = require('./mycrypto.js')

const hash = encrypt('Hello World!');

console.log(hash);

// {
//     iv: '237f306841bd23a418878792252ff6c8',
//     content: 'e2da5c6073dd978991d8c7cd'
// }

const text = decrypt(hash);

console.log(text); // Hello World!

// console.log(decrypt({
//     iv: 'fef78fb0c254c0e3e6bb3df3ae2342b4',
//     content: 'c900026f49d15209bf8124aacaa6467d5ad7d8812acbadc237f492fc1eb319c193d676'
// }))