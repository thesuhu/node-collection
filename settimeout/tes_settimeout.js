// setelah 5 detik baru muncul di console
const loggedInUser = 'John'

function greet(userName) {
    console.log('Welcome ' + userName + '!')
}
// setTimeout(greet, 5000, loggedInUser)


// dalam for loop
// beware ini di dalam cron akan overlaping
for (let x = 0; x < 10; x++) {
    setTimeout(function() {
        console.log(x)
    }, x * 1000)
}

// jika pakai cron, gunakan promise
async function counting() {
    for (let x = 0; x < 10; x++) {
        console.log(x)
        await timer(1000)
    }
}

const timer = ms => new Promise(res => setTimeout(res, ms))