const { Parser } = require('json2csv')

var person = [
    {
        "nama": "Jananto Sigit",
        "alamat": "Bekasi",
        "usia": "38"
    },
    {
        "nama": "Rudi Hartono",
        "alamat": "Cirebon",
        "usia": "36"
    },
    {
        "nama": "Alan Nurfitra",
        "alamat": "Jakarta",
        "usia": "26"
    },
    {
        "nama": "Umar Jati",
        "alamat": "40",
        "usia": "Depok",
    }
]

const json2csvParser = new Parser({ delimiter: '|', quote: '' })
const csv = json2csvParser.parse(person)

console.log(csv)

// const { Parser } = require('json2csv');

// const myCars = [
//   {
//     "car": "Audi",
//     "price": 40000,
//     "color": "blue"
//   }, {
//     "car": "BMW",
//     "price": 35000,
//     "color": "black"
//   }, {
//     "car": "Porsche",
//     "price": 60000,
//     "color": "green"
//   }
// ];

// const json2csvParser = new Parser();
// const csv = json2csvParser.parse(myCars);

// console.log(csv)