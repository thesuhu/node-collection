const fs = require('fs');
const pdfmake = require('pdfmake/build/pdfmake.js');
const pdffont = require('pdfmake/build/vfs_fonts.js');

pdfmake.vfs = pdffont.pdfMake.vfs;

let docDefinition = {
    content: [
        { text: 'Bidadari Penyelamat', style: 'header', alignment: 'left', fontSize: 18 },
        '\n',
        {
            text: `6.30 pagi
        Aku sudah terbangun
        Matahari bersinar
        Tapi tak terasa panas
        Setan dalam hati tertawa
        Teringat kejadian semalam
        Semua bilang aku salah jalan
        Tapi nggak ada satupun yang punya peta
        Haripun berlalu
        Tak sadar sudah tahunan
        Kemana bidadariku pergi
        Yang bisa menyelamatkanku
        Seharusnya dia dari tadi
        Sudah berada disini
        Malaikat di jiwa terbelenggu
        Akal sehatku pun mati
        Semua orang coba-cobai mau menolong
        Tapi terdengar seperti menggonggong
        Haripun berlalu
        Aku sudah kecebur semakin dalam`, alignment: 'left', fontSize: 12
        },
        '\n',
        { text: 'Slank - Album Minoritas - 1994', style: ['footer', 'anotherStyle'] },
    ],
    styles: {
        header: {
            fontSize: 18,
            bold: true
        },
        footer: {
            fontSize: 12,
        },
        anotherStyle: {
            italics: true,
            alignment: 'left'
        }
    }
};

const pdfDocGenerator = pdfmake.createPdf(docDefinition);
pdfDocGenerator.getBuffer((buffer) => {
    fs.writeFileSync('./temp/mypdf.pdf', buffer);
})

console.log('PDF created');