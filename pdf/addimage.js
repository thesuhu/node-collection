const { PDFDocument } = require('pdf-lib')
const fs = require('fs')
const assert = require('assert')
const path = require('path')

const run = async ({ pathToPDF, pathToImage }) => {
    const pdfDoc = await PDFDocument.load(fs.readFileSync(pathToPDF))
    // const img = await pdfDoc.embedPng(fs.readFileSync(pathToImage))
    const pngImage = await pdfDoc.embedPng(fs.readFileSync(pathToImage))
    // const imagePage = pdfDoc.insertPage(0)
    // const page = pdfDoc.addPage()

    // imagePage.drawImage(img, {
    //     x: 0,
    //     y: 0,
    //     width: imagePage.getWidth(),
    //     height: imagePage.getHeight()
    // })

    // Get the width/height of the PNG image scaled down to 50% of its original size
    // const pngDims = pngImage.scale(0.5)
    const pngDims = pngImage.scale(0.08)

    // // Draw the PNG image near the lower right corner of the JPG image
    // page.drawImage(pngImage, {
    //     x: page.getWidth() / 2 - pngDims.width / 2 + 75,
    //     y: page.getHeight() / 2 - pngDims.height,
    //     width: pngDims.width,
    //     height: pngDims.height,
    // })

    for (let i = 0; i < pdfDoc.getPageCount(); i++) {
        let page = ''
        page = pdfDoc.getPage(i)
        // console.log(i + 1)
        // console.log(page.getWidth())
        let xx = page.getWidth()
        // console.log(page.getHeight())
        let yy = page.getHeight()

        // // pojok kanan atas
        // page.drawImage(pngImage, {
        //     x: xx - pngDims.width - 10,
        //     y: yy - pngDims.height - 10,
        //     width: pngDims.width,
        //     height: pngDims.height
        // })

        // pojok kiri bawah
        // page.drawImage(pngImage, {
        //     x: 0 + pngDims.width,
        //     y: 0 + pngDims.height,
        //     width: pngDims.width,
        //     height: pngDims.height
        // })

        // pojok kanan bawah
        page.drawImage(pngImage, {
            x: xx - pngDims.width - 20,
            y: 0 + pngDims.height,
            width: pngDims.width,
            height: pngDims.height
        })
    }

    const pdfBytes = await pdfDoc.save();
    // const newFilePath = `${path.basename(pathToPDF, '.pdf')}-result.pdf`
    const fileParse = path.parse(pathToPDF)
    const newFilePath = fileParse.dir + '/' + fileParse.name + '-result.pdf'
    fs.writeFileSync(newFilePath, pdfBytes)
}

const ERRORS = {
    ARGUMENTS: 'Please provide a path to the PDF file as a first argument and path to an image as the second argument'
}

const pathToPDF = process.argv[2]
assert.notEqual(pathToPDF, null, ERRORS.ARGUMENTS)
const pathToImage = process.argv[3]
assert.notEqual(pathToImage, null, ERRORS.ARGUMENTS)

run({ pathToPDF, pathToImage }).catch(console.error)

// usage:
// node .\pdf\addimage.js  .\pdf\sample.pdf .\pdf\logo.png