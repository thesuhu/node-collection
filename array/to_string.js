var arrObj = [
  { 'FROM tblname': '*' },
  { 'FROM tblname': 'ERROR at line 27:' },
  {
    'FROM tblname': 'ORA-00942: table or view does not exist'
  },
  { 'FROM tblname': '' },
  { 'FROM tblname': '' }
]

console.log(arrObj)
console.log('type: ', typeof arrObj)

var jsonObj = arrObj[0]

console.log(jsonObj)
console.log('type: ', typeof jsonObj)


// convert to string
strObj = JSON.stringify(arrObj, null, 2)
console.log(strObj)
console.log(strObj.includes('ERROR at line'))