const {
    spawn
} = require('child_process')

// // A child process stdin is a writable stream. We can use it to send a command some input
// // Since the main process stdin is a readable stream, we can pipe that into a child process stdin stream

// const child = spawn('wc');

// process.stdin.pipe(child.stdin)

// child.stdout.on('data', (data) => {
//     console.log(`child stdout:\n${data}`);
// });

// child.stderr.on('data', (data) => {
//     console.error(`child stderr:\n${data}`);
// });

// child.on('exit', function(code, signal) {
//     console.log('child process exited with ' +
//         `code ${code} and signal ${signal}`)
// })

const find = spawn('find', ['.', '-type', 'f']);
const wc = spawn('wc', ['-l']);

find.stdout.pipe(wc.stdin);

wc.stdout.on('data', (data) => {
  console.log(`Number of files ${data}`);
});
