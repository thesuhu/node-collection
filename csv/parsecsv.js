const fs = require('fs')
const parse = require('csv-parse')

let objResult = {}
fs.createReadStream("./temp/Settlement SBS-0002-20220920215050129 20220927510964.csv")
    .pipe(parse({ delimiter: "_,", from_line: 1, to: 8, skip_empty_lines: true }))
    .on("data", function (row) {
        console.log(row);
        objResult[`${row[0].replace(/ /g, '_').toLowerCase()}`] = row[1]
    })
    .on("end", function () {
        console.log("finished");
        console.log(objResult)
        console.log(typeof objResult)
    })
    .on("error", function (error) {
        console.log(error.message);
    });

let header = ['no', 'merchant_name', 'payment_channel_name', 'transaction_date', 'invoice_number', 'customer_name', 'report_code', 'amount', 'recon_code', 'fee', 'pay_to_merchant', 'pay_out_date', 'transaction_type']
let objResultDetil = []
let objRow = {}
fs.createReadStream("./temp/Settlement SBS-0002-20220920215050129 20220927510964.csv")
    .pipe(parse({ delimiter: ",", from_line: 11, skip_empty_lines: true }))
    .on("data", function (row) {
        console.log(row);
        objRow = {}
        for (let i = 0; i < 13; i++) {
            objRow[`${header[i]}`] = row[i]
        }
        objResultDetil.push(objRow)
    })
    .on("end", function () {
        console.log("finished");
        console.log(objResultDetil)
        console.log(typeof objResultDetil)
    })
    .on("error", function (error) {
        console.log(error.message);
    });