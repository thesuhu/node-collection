const fs = require('fs')
const unzipper = require('unzipper')
const basedir = require('app-root-path')

const zipfile = basedir + '/zipunzip/temp/test-unzip.zip'
const zippassword = 'elsafrozen'
const targetdir = 'd:/Temporary'

unzip(zipfile, targetdir, zippassword)

// async function unzip(zipfile, targetDirectory, password) {
//     try {
//         const directory = await unzipper.Open.file(zipfile)

//         console.log('content: ', directory.files[0].path);

//         const extracted = await directory.files[0].stream(password).pipe(fs.createWriteStream(targetDirectory + '/firstfile.txt'))

//     } catch (err) {
//         console.log(err.message)
//     }
// }

async function unzip(zipfile, targetDirectory, password) {
    try {
        const directory = await unzipper.Open.file(zipfile)

        // console.log(directory)
        directory.files.forEach(async element => {
            console.log('content: ', element.path)
            const extracted = await element.stream(password).pipe(fs.createWriteStream(targetDirectory + '/' + element.path))
            console.log(extracted)
        })
    } catch (err) {
        console.log(err.message)
    }
}