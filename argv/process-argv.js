const {
    argv
} = require('process');

// print process.argv
argv.forEach((val, index) => {
    console.log(`${index}: ${val}`);
});

console.log('first name: ', argv[2], ', last name: ', argv[3])