require('dotenv').config()
// console.log(process.env)

const { oraexec } = require('oracledbexec')
var hasilAkhir = '' 

async function getparent() {
    try {
        var sql = `SELECT id,nmmenu FROM d_menu WHERE is_parent='1' ORDER BY id`
        var resParent = await oraexec(sql, {})
        var jmlParent = resParent.rows.length
        if (jmlParent > 0) {
            for (let i = 0; i < jmlParent; i++) {
                // console.log('parent: ', resParent.rows[i].NMMENU)
                var idParent = resParent.rows[i].ID
                hasilAkhir = hasilAkhir + 'parent ID: ' + idParent +': ' + resParent.rows[i].NMMENU + '\n'
                // kuncinya di sini agar wait, gunakan then
                await subMenu(idParent).then((resChild) => {
                    var jmlChild = resChild.rows.length
                    if (jmlChild > 0) {
                        for (j = 0; j < jmlChild; j++) {
                            // console.log('	child: ', resChild.rows[j].NMMENU)
                            hasilAkhir = hasilAkhir + '		child ID: ' + resChild.rows[j].ID + ': ' + resChild.rows[j].NMMENU + '\n'
                        }
                    }
                })
            }
        }
        console.log(hasilAkhir)
    } catch (err) {
        console.log(err.message)
    }
}

async function subMenu(parentId) {
    try {
        var sql = `SELECT id,nmmenu FROM d_menu WHERE parent_id=:parentId ORDER BY id`
        var resSql = await oraexec(sql, { parentId: parentId })
        return resSql
    } catch (err) {
        console.log(err.message)
    }
}

getparent()