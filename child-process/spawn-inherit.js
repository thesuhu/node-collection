// We can make the spawned child process inherit the standard IO objects of its parents if we want to, but also, more importantly, 
// we can make the spawn function use the shell syntax as well
const {
    spawn
} = require('child_process')

// const child = spawn('ls -l', {
//     stdio: 'inherit',
//     shell: true
// });

const child = spawn('ls -l | wc -l', {
  stdio: 'inherit',
  shell: true,
  cwd: 'd:/Temporary'
});