const {
    JsonDB
} = require('node-json-db')
const {
    Config
} = require('node-json-db/dist/lib/JsonDBConfig')

const db = new JsonDB(new Config("myDataBase", true, true, '/'))

db.push("/arraytest/myarray[]", {
    username: 'test4',
    salt: 'fafamarinisimarsisat',
    hash: 'wandalwasat'
}, true)

// var testString = db.getData("arraytest.myarray[0].username")
// console.log(testString)

var testString = db.getData("/arraytest/myarray")
console.log(testString)
console.log(testString.length)

var idx = db.getIndex("/arraytest/myarray", "test3", "username")
console.log(idx)

// db.delete("arraytest.myarray[" + db.getIndex("arraytest.myarray", "test4", "username") + "]")
// // note: delete index -1 akan menghapus baris terakhir 
// var testString = db.getData("arraytest.myarray")

// var testString = db.getData("arraytest.myarray")
// console.log(testString)