let date = new Date()
console.log('date: ', date)

// to EPOCH
let dateEpoch = date.valueOf()
console.log('long epoch: ', dateEpoch)

let localString = date.toLocaleString()
console.log('date: ', localString )
console.log(typeof localString)

console.log(date.getDay())
console.log(date.getDate())
console.log(date.getMonth())
console.log(date.getYear())
console.log(date.getFullYear())
